﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* Mike Al Jamal
 * Assignment2 Part2
 * 09/02/2019
 */

namespace A2MikeAlJamalP2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Volume of a sphere enter(1), Volume of a cylinder enter(2), Volume of a rectangular enter(3)");
            int num = int.Parse(Console.ReadLine());
            switch (num)
            {
                case 1:
                    Console.WriteLine("for volume of sphere enter Radius:");
                    double r = double.Parse(Console.ReadLine());
                    const double pi = 3.14159;
                    double spherevolume = 4 / 3 * pi * Math.Pow(r, 3);
                    Console.WriteLine("Sphere Volume: " + System.Math.Round(spherevolume,2));
                    Console.ReadLine();
                    break;
                case 2:
                    Console.WriteLine("for volume of cylinder enter Radius:");
                    double rc = double.Parse(Console.ReadLine());
                    Console.WriteLine("enter height:");
                    double h = double.Parse(Console.ReadLine());
                    double cylindervolume = h * pi * Math.Pow(rc, 2);
                    Console.WriteLine("Cylinder Volume: " + System.Math.Round(cylindervolume,2));
                    Console.ReadLine();
                    break;
                case 3:
                    Console.WriteLine("for volume of rectangular prism enter lenght:");
                    double lenght = double.Parse(Console.ReadLine());
                    Console.WriteLine("enter height:");
                    double height = double.Parse(Console.ReadLine());
                    Console.WriteLine("enter width:");
                    double width = double.Parse(Console.ReadLine());
                    double prismvolume = lenght * height * width;
                    Console.WriteLine("Rectangular Prism Volume: " + System.Math.Round(prismvolume,2));
                    Console.ReadLine();
                    break;
            }
        }
    }
}
