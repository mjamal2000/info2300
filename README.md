Step 1 - Make sure your computer is ready for Visual Studio
Before you begin installing Visual Studio:

Check the system requirements. These requirements help you know whether your computer supports Visual Studio 2017.

Apply the latest Windows updates. These updates ensure that your computer has both the latest security updates and the required system components for Visual Studio.

Reboot. The reboot ensures that any pending installs or updates don't hinder the Visual Studio install.

Free up space. Remove unneeded files and applications from your %SystemDrive% by, for example, running the Disk Cleanup app.

Check the system requirements. These requirements help you know whether your computer supports Visual Studio 2019.

Apply the latest Windows updates. These updates ensure that your computer has both the latest security updates and the required system components for Visual Studio.

Reboot. The reboot ensures that any pending installs or updates don't hinder the Visual Studio install.

Free up space. Remove unneeded files and applications from your %SystemDrive% by, for example, running the Disk Cleanup app.

For questions about running previous versions of Visual Studio side by side with Visual Studio 2017, see the Visual Studio compatibility details.

For questions about running previous versions of Visual Studio side by side with Visual Studio 2019, see the Visual Studio 2019 Platform Targeting and Compatibility page.

Step 2 - Download Visual Studio
Next, download the Visual Studio bootstrapper file.

To get a bootstrapper for Visual Studio 2017, see the Visual Studio previous versions download page for details on how to do so.

To do so, choose the following button, choose the edition of Visual Studio that you want, choose Save, and then choose Open folder.


Step 3 - Install the Visual Studio installer
Run the bootstrapper file to install the Visual Studio Installer. This new lightweight installer includes everything you need to both install and customize Visual Studio.

From your Downloads folder, double-click the bootstrapper that matches or is similar to one of the following files:

vs_community.exe for Visual Studio Community
vs_professional.exe for Visual Studio Professional
vs_enterprise.exe for Visual Studio Enterprise
If you receive a User Account Control notice, choose Yes.

We'll ask you to acknowledge the Microsoft License Terms and the Microsoft Privacy Statement. Choose Continue.

License Terms and Privacy Statement

Step 4 - Choose workloads
After the installer is installed, you can use it to customize your installation by selecting the feature sets—or workloads—that you want. Here's how.

Find the workload you want in the Visual Studio Installer.

Visual Studio 2017: Install a workload

For example, choose the ".NET desktop development" workload. It comes with the default core editor, which includes basic code editing support for over 20 languages, the ability to open and edit code from any folder without requiring a project, and integrated source code control.

After you choose the workload(s) you want, choose Install.

Next, status screens appear that show the progress of your Visual Studio installation.

Find the workload you want in the Visual Studio Installer.

Visual Studio 2019: Install a workload

For example, choose the "ASP.NET and web development" workload. It comes with the default core editor, which includes basic code editing support for over 20 languages, the ability to open and edit code from any folder without requiring a project, and integrated source code control.

After you choose the workload(s) you want, choose Install.

Next, status screens appear that show the progress of your Visual Studio installation.

Tip

At any time after installation, you can install workloads or components that you didn't install initially. If you have Visual Studio open, go to Tools > Get Tools and Features... which opens the Visual Studio Installer. Or, open Visual Studio Installer from the Start menu. From there, you can choose the workloads or components that you wish to install. Then, choose Modify.

Step 5 - Choose individual components (Optional)
If you don't want to use the Workloads feature to customize your Visual Studio installation, or you want to add more components than a workload installs, you can do so by installing or adding individual components from the Individual components tab. Choose what you want, and then follow the prompts.

Visual Studio 2017 - Install individual components

Visual Studio 2019 - Install individual components

Step 6 - Install language packs (Optional)
By default, the installer program tries to match the language of the operating system when it runs for the first time. To install Visual Studio in a language of your choosing, choose the Language packs tab from the Visual Studio Installer, and then follow the prompts.

Visual Studio 2017 - Install language packs

Visual Studio 2019 - Install language packs

Change the installer language from the command line
Another way that you can change the default language is by running the installer from the command line. For example, you can force the installer to run in English by using the following command: vs_installer.exe --locale en-US. The installer will remember this setting when it is run the next time. The installer supports the following language tokens: zh-cn, zh-tw, cs-cz, en-us, es-es, fr-fr, de-de, it-it, ja-jp, ko-kr, pl-pl, pt-br, ru-ru, and tr-tr.

Step 7 - Select the installation location (Optional)
New in 15.7: You can now reduce the installation footprint of Visual Studio on your system drive. You can choose to move the download cache, shared components, SDKs, and tools to different drives, and keep Visual Studio on the drive that runs it the fastest.

Visual Studio 2017 - Change installation locations

You can reduce the installation footprint of Visual Studio on your system drive. You can choose to move the download cache, shared components, SDKs, and tools to different drives, and keep Visual Studio on the drive that runs it the fastest.

Visual Studio 2019 - Select installation locations

Important

You can select a different drive only when you first install Visual Studio. If you've already installed it and want to change drives, you must uninstall Visual Studio and then reinstall it.

For more information, see the Select installation locations page.

Step 8 - Start developing
After Visual Studio installation is complete, choose the Launch button to get started developing with Visual Studio.

Choose File, and then choose New Project.

Select a project type.

For example, to build a C++ app, choose Installed, expand Visual C++, and then choose the C++ project type that you want to build.

To build a C# app, choose Installed, expand Visual C#, and then choose the C# project type that you want to build.

After Visual Studio installation is complete, choose the Launch button to get started developing with Visual Studio.

On the start window, choose Create a new project.

In the search box, enter the type of app you want to create to see a list of available templates. The list of templates depends on the workload(s) that you chose during installation. To see different templates, choose different workloads.

You can also filter your search for a specific programming language by using the Language drop-down list. You can filter by using the Platform list and the Project type list, too.

Visual Studio opens your new project, and you're ready to code!

Licence Choice: open source licencing agreement because the software is not for profit.